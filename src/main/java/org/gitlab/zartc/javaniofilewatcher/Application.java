package org.gitlab.zartc.javaniofilewatcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import lombok.extern.log4j.Log4j2;


@Log4j2
@Command(name = "watch", mixinStandardHelpOptions = true, version = "1.0")
public class Application implements Runnable {

    @Parameters(description = "Any number of directory path to watch")
    private final List<String> dirToWatch = new ArrayList<>();


    public void run() {
        try {
            StopWatch stopWatch = StopWatch.createStarted();

            // the queue where the FileWatcher will put files as it detects them
            var queue = new LinkedBlockingQueue<File>();

            // the filter for the FileWatcher
            var filter = FileFilterUtils.makeFileOnly(FileFilterUtils.suffixFileFilter(".csv"));

            // the FileWatcher
            var fileWatcher = new FileWatcher(filter, dirToWatch, queue);

            // start the FileWatcher in a new thread
            new Thread(fileWatcher, "file-watcher").start();

            // log the time it takes to init
            log.info("Started in ............. : {}", DurationFormatUtils.formatDuration(stopWatch.getTime(), "ss.SSS 'seconds'"));

            // infinite loop
            while (!Thread.interrupted()) {
                // wait for a File to appear in the shared queue
                var file = queue.take();

                // consume it
                log.info("  processing ........... : {}", file);
            }
        } catch (Exception e) {
            log.catching(e);
        }
    }


    public static void main(String[] args) {
        new CommandLine(new Application()).execute(args);
    }
}
