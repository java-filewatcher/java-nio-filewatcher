package org.gitlab.zartc.javaniofilewatcher;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.io.filefilter.IOFileFilter;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class FileWatcher implements Runnable {
    private final IOFileFilter filter;
    private final WatchService watchService;
    private final BlockingQueue<File> queue;


    public FileWatcher(IOFileFilter filter, List<String> watches, BlockingQueue<File> queue) throws IOException {
        this.watchService = FileSystems.getDefault().newWatchService();
        this.filter = filter;
        this.queue = queue;

        log.info("filter in effect ....... : {}", filter.toString());

        for (String s : watches) {
            var dir = new File(s);

            if (!dir.exists() || !dir.isDirectory()) {
                throw new Error("path '%s' does not exists or is not a directory".formatted(dir.getAbsolutePath()));
            }

            dir.toPath().register(watchService, ENTRY_CREATE);
            log.info("watching ............... : {}", dir.getAbsolutePath());
        }
    }

    public void run() {
        try {
            WatchKey key;

            do {
                try {
                    // wait for key to be signaled
                    key = watchService.take();

                    if (Thread.interrupted()) {
                        return;
                    }

                    if (key.isValid()) {
                        process(key);
                    }
                } catch (InterruptedException x) {
                    return;
                }

                // Reset the key -- this step is critical if you want to receive further watch events.
            } while (key.reset());
        } catch (Exception e) {
            log.catching(e);
        }
    }

    private void process(WatchKey key) {
        var watchable = (Path)key.watchable();

        for (WatchEvent<?> event : key.pollEvents()) {
            // OVERFLOW event can occur if events are lost or discarded.
            // If the event count is greater than 1 then this is a repeated event
            if ((event.kind() == OVERFLOW) || (event.count() > 1)) {
                continue;
            }

            var file = watchable.resolve((Path)event.context()).toFile();

            if (filter.accept(file)) {
                log.debug("accepted event {} for '{}'", event.kind(), file);
                queue.offer(file);
            } else {
                log.debug("rejected event {} for '{}' (file does not match {})", event.kind(), file, filter.toString());
            }
        }
    }
}

/* EOF */
